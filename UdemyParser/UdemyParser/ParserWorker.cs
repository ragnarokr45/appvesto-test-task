﻿using AngleSharp.Html.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdemyParser
{
    class ParserWorker<T> where T : class
    {
        IParser<T> parser;
        IParserSettings settings;

        HtmlLoader loader;

        public event Action<object, T> OnCompleted;


        public ParserWorker(IParser<T> parser, IParserSettings settings)
        {
            this.parser = parser;
            this.settings = settings;
            loader = new HtmlLoader(settings);
        }
        
        public async  void Worker()
        {
            var source = await loader.GetSource();
            var result = parser.Parse(source);
            OnCompleted?.Invoke(this, result);
        }
    }
}
