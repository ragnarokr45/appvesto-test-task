﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdemyParser
{
    interface IParserSettings
    {
        string BaseUrl { get; set; }
        string NameOfCourse { get; set; }
    }
}
