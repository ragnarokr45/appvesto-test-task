﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdemyParser.FlutterCourse;

namespace UdemyParser
{
    class Program
    {
        
        static void Main(string[] args)
        {
            ParserWorker<string[]> parser = new ParserWorker<string[]>(
                                            new FlutterCourseParser(), 
                                            new FlutterCourseSettings());
            parser.OnCompleted += Parser_OnCompleted;
            parser.Worker();

            Console.ReadKey();
        }

        private static void Parser_OnCompleted(object arg1, string[] arg2)
        {
            foreach(var p in arg2)
            {
                Console.WriteLine(p);
            }
        }
    }
}
