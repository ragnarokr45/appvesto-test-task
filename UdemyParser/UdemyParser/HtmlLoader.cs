﻿using AngleSharp;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace UdemyParser
{

    class HtmlLoader
    {
       // readonly HttpClient client;
        readonly string url;

        public HtmlLoader(IParserSettings settings)
        {
            url = $"{settings.BaseUrl}/{settings.NameOfCourse}/";
         //   client = new HttpClient();
        }

        public async Task<AngleSharp.Dom.IDocument> GetSource()
        {
            var config = new Configuration().WithDefaultLoader();
            var context = BrowsingContext.New(config);
            var source = await context.OpenAsync(url);
            return source;
        }
    }
}
