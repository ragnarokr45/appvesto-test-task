﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdemyParser.FlutterCourse
{
    class FlutterCourseSettings : IParserSettings
    {

        public string BaseUrl { get; set; } = "https://www.udemy.com/course";
        public string NameOfCourse { get; set; } = "learn-flutter-dart-to-build-ios-android-apps";
    }
}
