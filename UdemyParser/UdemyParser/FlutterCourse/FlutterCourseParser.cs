﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;

namespace UdemyParser
{
    class FlutterCourseParser : IParser<string[]>
    {

        public string[] Parse(IDocument document)
        {
            var courseList = new List<string>();
            var resultList = new List<string>();

            var items = document.QuerySelectorAll("div.title > a , div.details > a + span.content-summary");

            foreach (var c in items)
            {
                courseList.Add(c.TextContent.Trim());
                courseList.RemoveAll(x => x == string.Empty);
            }

            for (int i = 0; i < courseList.Count; i += 2)
            {
                resultList.Add(courseList[i + 1] + " " + courseList[i]);
            }
            resultList.Sort();
            return resultList.ToArray();
        }
    }
}
