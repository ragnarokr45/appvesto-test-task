﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnightsTourProblem
{
    class Program
    {
        static int Solution(int x, int y, int[,] position)
        {
            int t = 0;
            if (x > 0 && y > 1 && position[x - 1, y - 2] == 0) t++;
            if (x > 0 && y < 6 && position[x - 1, y + 2] == 0) t++;
            if (x > 1 && y > 0 && position[x - 2, y - 1] == 0) t++;
            if (x > 1 && y < 7 && position[x - 2, y + 1] == 0) t++;
            if (x < 7 && y > 1 && position[x + 1, y - 2] == 0) t++;
            if (x < 7 && y < 6 && position[x + 1, y + 2] == 0) t++;
            if (x < 6 && y > 0 && position[x + 2, y - 1] == 0) t++;
            if (x < 6 && y < 7 && position[x + 2, y + 1] == 0) t++;
            return t;
        }
        static void Main(string[] args)
        {
            int[,] position = new int[8, 8];
            int i = 0;
            int j = 0;
            int countOfMoves = 0, x, y;
            int tmp, tmp1;

            Console.WriteLine("x = ");
            x = Convert.ToInt32(Console.ReadLine());
            x--;
            Console.WriteLine("y = ");
            y = Convert.ToInt32(Console.ReadLine());
            y--;
            while (countOfMoves < 64)
            {
                countOfMoves++;
                position[x, y] = countOfMoves;
                tmp = 9;

                if (x > 0 && y > 1 && position[x - 1, y - 2] == 0)
                {
                    tmp1 = Solution(x - 1, y - 2, position);
                    if (tmp1 < tmp) { i = x - 1; j = y - 2; tmp = tmp1; }
                }
                if (x > 0 && y < 6 && position[x - 1, y + 2] == 0)
                {
                    tmp1 = Solution(x - 1, y + 2, position);
                    if (tmp1 < tmp) { i = x - 1; j = y + 2; tmp = tmp1; }
                }
                if (x > 1 && y > 0 && position[x - 2, y - 1] == 0)
                {
                    tmp1 = Solution(x - 2, y - 1, position);
                    if (tmp1 < tmp) { i = x - 2; j = y - 1; tmp = tmp1; }
                }
                if (x > 1 && y < 7 && position[x - 2, y + 1] == 0)
                {
                    tmp1 = Solution(x - 2, y + 1, position);
                    if (tmp1 < tmp) { i = x - 2; j = y + 1; tmp = tmp1; }
                }
                if (x < 7 && y > 1 && position[x + 1, y - 2] == 0)
                {
                    tmp1 = Solution(x + 1, y - 2, position);
                    if (tmp1 < tmp) { i = x + 1; j = y - 2; tmp = tmp1; }
                }
                if (x < 7 && y < 6 && position[x + 1, y + 2] == 0)
                {
                    tmp1 = Solution(x + 1, y + 2, position);
                    if (tmp1 < tmp) { i = x + 1; j = y + 2; tmp = tmp1; }
                }
                if (x < 6 && y > 0 && position[x + 2, y - 1] == 0)
                {
                    tmp1 = Solution(x + 2, y - 1, position);
                    if (tmp1 < tmp) { i = x + 2; j = y - 1; tmp = tmp1; }
                }
                if (x < 6 && y < 7 && position[x + 2, y + 1] == 0)
                {
                    tmp1 = Solution(x + 2, y + 1, position);
                    if (tmp1 < tmp) { i = x + 2; j = y + 1; tmp = tmp1; }
                }

                x = i;
                y = j;
            }

            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                    Console.Write("\t" + position[i, j]);
                Console.WriteLine("\n");
            }

            Console.ReadKey();
        }
    }
}
